# Releases
## Alpha Version - v0.6
### New features

### Improved / Changed

## Alpha Version - v0.5
### New features
- Support for localization in the minigame information screen
- Support for localization in minigame descriptions
- Translated minigames
- Improved the API for board events (green spaces)
- Add music to:
  - The test and KDEValley boards
  - The Escape from lava minigame

### Improved / Changed
- Fix French language not selectable
- Fixed the position of the minigame information screen to cover the full window
- Improved icon quality
- Fixed a bug that caused the cake icon on cake spots to disappear
- Fixed a crash when player landed on a trap
- Fixed a bug that caused the shop not to open when landing directly on it
- The characters in the knock off minigame now face the direction they are walking
- Made the options menu accessible from the pause menu look like in the main menu
- Improved the controller navigation in the options
- The characters in the harvest food minigame no longer spawn in the air
- Fixed the black outline on the green tux texture
- Fixed the descriptions in the main menu back buttons

## Alpha Version - v0.4
### New features
- Support for localization (except plugins)
  - Currently supported languages:
    - English
    - Brazilian Portuguese
    - German
    - French
- Team indicator in 2v2 minigames

### Improved / Changed
- The main menu can now be navigated with keyboard/controller
- The board overlay now shows the items of each player
- Computer controlled characters now buy items in the shop
- Added Music in the main menu
- Fix a bug that made items not usable in games loaded from savegames
- Added textures for Harvest Food minigame and placement scene

Internally the project has switched to the new [Godot](https://godotengine.org) version 3.1

## Alpha Version - v0.3 - 2019-02-02
### New features
- KDEValley, a new board
- Escape from lava, a new minigame
- New background for main menu
- New scenery for 'test' board
- Added screenshot for 'Bowling minigame'
- Frame cap and VSync can now be set in options
- Items (e.g. Dice and traps)
- Items can be bought in Shop Spaces (purple color)

### Improved / Changed
- Each character can only be chosen once now
- New GUI theme
- Options can now be opened in-game
- Smooth rotations for board movement

Internally the project has been restructured and switched to the
[Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows).

## Alpha Version - v0.2 - 2018-10-31
### New features
- Godette, Godot's unofficial mascot, as a new playable character, !53
- Animations for all characters, !25
- Bowling minigame, one player tries to hit the other 3 players, !63
- Kernel Compiling minigame, press the correct buttons quickly, !52
- Harvest Food minigame, guessing game, !39
- Boards can now have multiple paths, players can choose which way to go, !26
- Minigame information screens, !28
- New gamemodes added such as, Duel, 1v3 and 2v2, !64
- Games can now be saved, !33

### Improved
- Options are now saved, !54
- Fixed a memory leak, !29
- Improved mesh and texture for ice in Knock Off minigame, !30, !34
- Hurdle minigame now has powerups and different hurdles, !38

## Demo Version - v0.1 - 2018-09-01
- 3 playable characters
- 2 minigames
- 2 reward systems, winner takes all and a linear one
- 1 board
- AI opponents
- Controller remapping
- Dynamic loading of boards & characters
